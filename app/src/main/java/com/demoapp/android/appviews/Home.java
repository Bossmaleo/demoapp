package com.demoapp.android.appviews;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.demoapp.android.R;
import com.demoapp.android.connInscript.MainActivity;
import com.demoapp.android.model.SessionManager;

public class Home extends AppCompatActivity {

    private TextView nom;
    private TextView prenom;
    private TextView datenaissance;
    private TextView email;
    private TextView telephone;
    private SessionManager session;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        session = new SessionManager(this);

        nom = findViewById(R.id.nom);
        prenom = findViewById(R.id.prenom);
        datenaissance = findViewById(R.id.datenaissance);
        email = findViewById(R.id.email);
        telephone = findViewById(R.id.telephone);

        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Accueil");

        nom.setText("NOM : "+String.valueOf(session.getUserDetail().get(SessionManager.Key_NOM)));
        prenom.setText("PRENOM : "+String.valueOf(session.getUserDetail().get(SessionManager.Key_PRENOM)));
        datenaissance.setText("DATE DE NAISSANCE : "+String.valueOf(session.getUserDetail().get(SessionManager.Key_DATENAISSANCE)));
        email.setText("EMAIL : "+String.valueOf(session.getUserDetail().get(SessionManager.Key_EMAIL)));
        telephone.setText("TELEPHONE : "+String.valueOf(session.getUserDetail().get(SessionManager.Key_TELEPHONE)));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem favoriteItem = menu.findItem(R.id.deco);
        Drawable newIcon = favoriteItem.getIcon();
        newIcon.mutate().setColorFilter(Color.rgb(255, 255, 255), PorterDuff.Mode.SRC_IN);
        favoriteItem.setIcon(newIcon);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.deco:
                session.logoutUser();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
    }
}
