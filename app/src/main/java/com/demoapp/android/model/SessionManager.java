package com.demoapp.android.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "Colispref";

    public static final String Key_NOM = "NOM";
    public static final String Key_PRENOM = "PRENOM";
    public static final String Key_DATENAISSANCE = "DATENAISSANCE";
    public static final String Key_EMAIL = "EMAIL";
    public static final String Key_TELEPHONE = "TELEPHONE";

    private static final String IS_LOGIN = "IsLoggedIn";

    public SessionManager(Context context)
    {
        this._context = context;
        pref = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String nom,String prenom,String datenaissance,String telephone,String email)
    {
        editor.putBoolean(IS_LOGIN,true);
        editor.putString(Key_NOM, nom);
        editor.putString(Key_PRENOM, prenom);
        editor.putString(Key_DATENAISSANCE, datenaissance);
        editor.putString(Key_TELEPHONE, telephone);
        editor.putString(Key_EMAIL, email);

        editor.commit();
    }

    public HashMap<String,String> getUserDetail()
    {
        HashMap<String,String> user = new HashMap<String,String>();
        user.put(Key_NOM,pref.getString(Key_NOM,null));
        user.put(Key_PRENOM,pref.getString(Key_PRENOM,null));
        user.put(Key_DATENAISSANCE,pref.getString(Key_DATENAISSANCE,null));
        user.put(Key_EMAIL,pref.getString(Key_EMAIL,null));
        user.put(Key_TELEPHONE,pref.getString(Key_TELEPHONE,null));
        return user;
    }

    public void logoutUser()
    {
        editor.clear();
        editor.commit();
    }

    public boolean IsLoggedIn()
    {
        return pref.getBoolean(IS_LOGIN,false);
    }

}
