package com.demoapp.android.connInscript;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.demoapp.android.R;
import com.demoapp.android.appviews.Home;
import com.demoapp.android.model.SessionManager;

public class MainActivity extends AppCompatActivity {
    private SessionManager session;
    private RelativeLayout block;
    //private Profil user;
    //private DatabaseHandler database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new SessionManager(this);

        Thread background = new Thread() {
            public void run() {

                try {
                    sleep(3*1000);
                    finish();
                    if(!session.IsLoggedIn()) {
                        Intent i = new Intent(getApplicationContext(), Connexion.class);
                        startActivity(i);
                        finish();
                    }else {
                        Intent i = new Intent(getApplicationContext(), Home.class);
                        startActivity(i);
                        finish();
                    }
                    finish();

                } catch (Exception e) {

                }
            }
        };

        background.start();
    }


}
