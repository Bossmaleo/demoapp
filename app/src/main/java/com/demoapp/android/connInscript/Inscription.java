package com.demoapp.android.connInscript;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demoapp.android.R;
import com.demoapp.android.appviews.Home;
import com.demoapp.android.model.SessionManager;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Inscription extends AppCompatActivity {

    private int day;
    private int month;
    private int year;
    static final int DATE_DIALOG_ID = 999;

    private EditText nom;
    private EditText prenom;
    private EditText date_de_naissance;
    private EditText telephone;
    private EditText email;
    private MaterialButton suivant;
    private EditText password;
    private EditText passwordconfirm;
    private MaterialButton terminer;

    private TextInputLayout nom_error;
    private TextInputLayout prenom_error;
    private TextInputLayout datenaissance_error;
    private TextInputLayout telephone_error;
    private TextInputLayout email_error;
    private TextInputLayout password_error;
    private TextInputLayout passwordconfirm_error;

    RelativeLayout blockview1;
    RelativeLayout blockview2;

    private Animation anim;
    private Toolbar toolbar;
    private ProgressDialog pDialog;
    private JSONObject reponse;
    private SessionManager session;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription);

        nom = findViewById(R.id.nom);
        prenom = findViewById(R.id.prenom);
        date_de_naissance = findViewById(R.id.datenaissance);
        telephone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        suivant = findViewById(R.id.ajouter1);
        password = findViewById(R.id.password);
        passwordconfirm = findViewById(R.id.password_confirm);
        terminer = findViewById(R.id.terminer);


        nom_error = findViewById(R.id.text_input_layout_nom);
        prenom_error = findViewById(R.id.text_input_layout_prenom);
        datenaissance_error = findViewById(R.id.text_input_layout_datenaissance);
        telephone_error = findViewById(R.id.text_input_layout_phone);
        email_error = findViewById(R.id.text_input_layout_email);
        password_error = findViewById(R.id.text_input_layout_password);
        passwordconfirm_error = findViewById(R.id.text_input_layout_password_confirm);
        blockview1 = findViewById(R.id.block);
        blockview2 = findViewById(R.id.block2);

        toolbar =  findViewById(R.id.toolbar);
        session = new SessionManager(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Inscription");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setCurrentDateOnView();

        anim = AnimationUtils.loadAnimation(this,R.anim.slide_in);

        date_de_naissance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        date_de_naissance.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    showDialog(DATE_DIALOG_ID);
                }
            }
        });

        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate1()==true){
                    blockview1.setVisibility(View.GONE);
                    blockview2.setVisibility(View.VISIBLE);
                    blockview2.startAnimation(anim);
                }
            }
        });

        terminer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate2()==true){
                    pDialog = new ProgressDialog(Inscription.this);
                    pDialog.setMessage("Connexion en cours...");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(false);
                    pDialog.show();
                    ConnexionInsertion();
                }
            }
        });
    }

    public boolean validate1() {
        boolean valid = true;

        String _email = email.getText().toString();
        String _nom = nom.getText().toString();
        String _prenom = prenom.getText().toString();
        String _datenaissance = date_de_naissance.getText().toString();
        String _phone = telephone.getText().toString();

        if (_email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(_email).matches()) {
            email_error.setError("Votre email est incorrect");
            valid = false;
        } else {
            email_error.setError(null);
        }

        if (_nom.isEmpty()) {
            nom_error.setError("Veuillez renseigner votre nom");
            valid = false;
        } else {
            nom_error.setError(null);
        }

        if (_prenom.isEmpty()) {
            prenom_error.setError("Veuillez renseigner votre prenom");
            valid = false;
        } else {
            prenom_error.setError(null);
        }

        if (_datenaissance.isEmpty()) {
            datenaissance_error.setError("Veuillez renseigner votre date de naissance");
            valid = false;
        } else {
            datenaissance_error.setError(null);
        }

        if(_phone.isEmpty()) {
            telephone_error.setError("Veuillez renseigner Votre date de naissance");
        } else {
            telephone_error.setError(null);
        }

        return valid;
    }

    public boolean validate2() {
        boolean valid = true;

        String _password = password.getText().toString();
        String _passwordconfirm = passwordconfirm.getText().toString();

        if (_password.isEmpty()) {
            password_error.setError("Veuillez renseigner votre prenom");
            valid = false;
        } else {
            password_error.setError(null);
        }

        if (!_password.equals(_passwordconfirm)) {
            passwordconfirm_error.setError("Les deux mots de passe sont differents");
            valid = false;
        } else {
            passwordconfirm_error.setError(null);
        }



        return valid;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month,day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth+1;
            day = selectedDay;

            if(month>9 && day>9) {
                date_de_naissance.setText(day + "/" + month + "/" + year);
            }else if(day<9 && month>9)
            {
                date_de_naissance.setText("0"+day + "/" + month + "/" + year);
            }else if(month<9 && day>9)
            {
                date_de_naissance.setText(day + "/0" + month + "/" + year);
            }
            else if(month<9 && day<9)
            {
                date_de_naissance.setText("0"+day + "/0" + month + "/" + year);
            }
        }
    };



    public void setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // User chose the "Settings" item, show the app settings UI...
                Intent i = new Intent();
                setResult(RESULT_OK, i);
                finish();
                return true;



            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        setResult(RESULT_OK, i);
        finish();
    }

    private void ConnexionInsertion()
    {
        String naissance = String.valueOf(date_de_naissance.getText().toString()).split("/")[2]+"-"
                +String.valueOf(date_de_naissance.getText().toString()).split("/")[1]+"-"
                +String.valueOf(date_de_naissance.getText().toString()).split("/")[0];
        String url_connexion = "http://wazzaby.com/DemoApp/insertion.php?NOM=" + (nom.getText().toString())
                +"&PRENOM="+ (prenom.getText().toString())+"&TELEPHONE="+(telephone.getText().toString())
                +"&EMAIL="+(email.getText().toString())+"&PASSWORD="+(password.getText().toString())
                +"&DATENAISSANCE="+(naissance);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_connexion,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        showJSON(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        if(error instanceof ServerError)
                        {
                            Toast.makeText(Inscription.this,"Une erreur au niveau du serveur viens de survenir ",Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }else if(error instanceof NetworkError)
                        {
                            Toast.makeText(Inscription.this,"Une erreur  du réseau viens de survenir ",Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }else if(error instanceof AuthFailureError)
                        {
                            Toast.makeText(Inscription.this,"Une erreur d'authentification réseau viens de survenir ",Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }else if(error instanceof ParseError)
                        {
                            Toast.makeText(Inscription.this,"Une erreur  du réseau viens de survenir ",Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }else if(error instanceof NoConnectionError)
                        {
                            Toast.makeText(Inscription.this,"Une erreur  du réseau viens de survenir, veuillez revoir votre connexion internet ",Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }else if(error instanceof TimeoutError)
                        {
                            Toast.makeText(Inscription.this,"Le delai d'attente viens d'expirer,veuillez revoir votre connexion internet ! ",Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }else
                        {

                            Toast.makeText(Inscription.this,"Une erreur  du réseau viens de survenir ", Toast.LENGTH_LONG).show();
                            email.setText("");
                            password.setText("");
                        }
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("email",email.getText().toString());
                params.put("password",password.getText().toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void showJSON(String response) {
        try {
            reponse = new JSONObject(response);
            /*session.createLoginSession(reponse.getString("NOM")
                    ,reponse.getString("PRENOM")
                    ,reponse.getString("DATENAISSANCE")
                    ,reponse.getString("TELEPHONE")
                    ,reponse.getString("EMAIL"));*/
            Intent intent = new Intent(getApplicationContext(), Connexion.class);
            startActivity(intent);
            Toast.makeText(Inscription.this,"Votre inscription vient de reussir avec succes !!",Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
